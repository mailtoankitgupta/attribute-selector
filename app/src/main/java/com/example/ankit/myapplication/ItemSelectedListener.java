package com.example.ankit.myapplication;

public interface ItemSelectedListener {
    void onItemSelected(int attributePosition, int attributeOptionPosition);
}