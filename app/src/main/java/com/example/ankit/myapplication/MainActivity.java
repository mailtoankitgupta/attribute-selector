package com.example.ankit.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements ItemSelectedListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private static final String mAttributes[] = {"Size", "Color", "Printing Style"};

    private static final String mAttributeOptions[][] = {
            {"3x6", "4x8", "6x12", "7x8", "6x5", "5x9"},
            {"Red", "Blue", "Green", "Yellow", "Orange", "Pink"},
            {"Glossy", "Matt"}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new AttributeAdapter(mAttributes, this, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemSelected(int attributePosition, int attributeOptionPosition) {
        // Gives the currently selected values for the attributes;
        // Maintain a store for the attribute=>attributeOption and update when this function is called
        Toast.makeText(this, "Selected: " + attributePosition + ", " + attributeOptionPosition, Toast.LENGTH_SHORT).show();
    }

    public static class AttributeAdapter extends RecyclerView.Adapter<AttributeAdapter.ViewHolder> {
        private String[] mAttributes;
        private Context mContext;
        private ItemSelectedListener mListener;
        private RecyclerView.Adapter mAttributeOptionsAdapter;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public static class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public RecyclerView mRecyclerView;
            public ViewHolder(RecyclerView v) {
                super(v);
                mRecyclerView = v;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public AttributeAdapter(String[] myDataset, Context context, ItemSelectedListener listener) {
            mAttributes = myDataset;
            mContext = context;
            mListener = listener;
        }

        @Override
        public AttributeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView v = (RecyclerView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_recycler_view, parent, false);
            // set the view's size, margins, paddings and layout parameters
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            v.setHasFixedSize(true);

            // use a linear layout manager
            v.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            AttributeAdapter.ViewHolder  vh = new AttributeAdapter.ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final AttributeAdapter.ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            // TODO: Optimize this to store in memory
            mAttributeOptionsAdapter = new AttributeOptionsAdapter(mAttributeOptions[position], mContext, mListener, position);
            holder.mRecyclerView.setAdapter(mAttributeOptionsAdapter);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mAttributes.length;
        }
    }
}