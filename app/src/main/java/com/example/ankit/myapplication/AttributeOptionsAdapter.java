package com.example.ankit.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AttributeOptionsAdapter extends RecyclerView.Adapter<AttributeOptionsAdapter.ViewHolder> {
    private String[] mDataset;
    private int mSelected = 0;
    private TextView mSelectedView;
    private Context mContext;
    private ItemSelectedListener mListener;
    private int mAttributePosition;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(TextView v) {
            super(v);
            mTextView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AttributeOptionsAdapter(String[] myDataset, Context context, ItemSelectedListener listener, int position) {
        mDataset = myDataset;
        mContext = context;
        mListener = listener;
        mAttributePosition = position;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AttributeOptionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset[position]);
        if (mSelected == position) {
            onSelectChange(holder.mTextView, null);
        } else {
            holder.mTextView.setBackground(null);
        }

        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelected = holder.getAdapterPosition();
                onSelectChange((TextView) view, mSelectedView);
            }
        });
    }

    private void onSelectChange(TextView txtNew, TextView txtPrev) {
        if (txtPrev != null) {
            txtPrev.setBackground(null);
        }
        txtNew.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        mSelectedView = txtNew;
        // Call the activity to inform about the new selection
        mListener.onItemSelected(mAttributePosition, mSelected);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}